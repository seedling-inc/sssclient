import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

@Component({
  selector: 'app-overview-tbl',
  templateUrl: './overview-tbl.component.html',
  styleUrls: ['./overview-tbl.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OverviewTblComponent implements OnInit {

  @Input() dataSource: any;
  @Input() displayedColumns: any;


  constructor() { }

  ngOnInit(): void {
  }

}
