import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OverviewTblComponent } from './overview-tbl.component';

describe('OverviewTblComponent', () => {
  let component: OverviewTblComponent;
  let fixture: ComponentFixture<OverviewTblComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OverviewTblComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewTblComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
